namespace :airports do
  desc 'Seed the airports from the json file'
  task seed: :environment do
    path = Rails.root.join('config', 'alaska_airports.json')

    json = JSON.parse(File.read(path))

    json.each do |airport|
      month, day, year = airport['EffectiveDate'].split('/').map(&:to_i)

      if year < 20
        year += 2000
      else
        year += 1900
      end

      effective_date = Date.new(year, month, day)

      Airport.create!(
        site_number: airport['SiteNumber'],
        site_type: airport['Type'],
        location_id: airport['LocationID'],
        effective_date: effective_date,
        region: airport['Region'],
        state: airport['State'],
        state_name: airport['StateName'],
        county: airport['County'],
        county_state: airport['CountyState'],
        city: airport['City'],
        facility_name: airport['FacilityName'],
        lat: airport['Lat'].to_f,
        lon: airport['Lon'].to_f
      )
    end
  end
end
