namespace :airports do
  desc 'Calculate all the distances'
  task update_distance: :environment do
    combinations = Airport.pluck(:id).permutation(2).to_a

    combinations.each do |origin_id, destination_id|
      next if AirportDistance.where(origin_id: origin_id, destination_id: destination_id).exists?

      origin = Airport.find(origin_id)
      destination = Airport.find(destination_id)

      distance = origin.distance_to(destination)

      AirportDistance.create(origin: origin, destination: destination, distance: distance)
      AirportDistance.create(origin: destination, destination: origin, distance: distance)
    end
  end
end
