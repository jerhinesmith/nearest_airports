class Airport < ApplicationRecord
  has_many :airport_distances, foreign_key: :origin_id
  has_many :destinations, through: :airport_distances

  def distance_to(airport)
    r = 6_371_000 # meters

    φ1 = degrees_to_radians(lat)
    φ2 = degrees_to_radians(airport.lat)

    Δφ = degrees_to_radians(airport.lat - lat)
    Δλ = degrees_to_radians(airport.lon - lon)

    a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
        Math.cos(φ1) * Math.cos(φ2) *
        Math.sin(Δλ / 2) * Math.sin(Δλ / 2)

    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

    r * c
  end

  private

  def degrees_to_radians(degrees)
    degrees.to_f * Math::PI / 180
  end
end
