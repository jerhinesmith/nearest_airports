class AirportDistance < ApplicationRecord
  belongs_to :origin, class_name: 'Airport'
  belongs_to :destination, class_name: 'Airport'

  scope :closest, -> (number) { order(:distance).limit(number) }
  scope :within, -> (kilometers) { where(AirportDistance.arel_table[:distance].lteq(kilometers * 1_000)) }
end
