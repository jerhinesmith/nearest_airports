class WelcomeController < ApplicationController
  def show
    if params[:id]
      @airport = Airport.find(params[:id])
    end
  end
end
