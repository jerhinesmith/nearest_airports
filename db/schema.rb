# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_25_215528) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "airport_distances", force: :cascade do |t|
    t.integer "origin_id", null: false
    t.integer "destination_id", null: false
    t.float "distance", default: 0.0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["destination_id"], name: "index_airport_distances_on_destination_id"
    t.index ["origin_id", "destination_id"], name: "index_airport_distances_on_origin_id_and_destination_id", unique: true
  end

  create_table "airports", force: :cascade do |t|
    t.string "site_number", null: false
    t.string "site_type", null: false
    t.string "location_id", null: false
    t.date "effective_date", null: false
    t.string "region", null: false
    t.string "state", null: false
    t.string "state_name", null: false
    t.string "county", null: false
    t.string "county_state", null: false
    t.string "city", null: false
    t.string "facility_name", null: false
    t.float "lat", null: false
    t.float "lon", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
