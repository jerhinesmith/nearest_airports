class CreateAirportDistances < ActiveRecord::Migration[5.2]
  def change
    create_table :airport_distances do |t|
      t.integer :origin_id, null: false
      t.integer :destination_id, null: false
      t.float :distance, null: false, default: 0.0

      t.timestamps
    end

    add_index :airport_distances, [:origin_id, :destination_id], unique: true
    add_index :airport_distances, :destination_id
  end
end
