class CreateAirports < ActiveRecord::Migration[5.2]
  def change
    create_table :airports do |t|
      t.string :site_number, null: false
      t.string :site_type, null: false
      t.string :location_id, null: false
      t.date :effective_date, null: false
      t.string :region, null: false
      t.string :state, null: false
      t.string :state_name, null: false
      t.string :county, null: false
      t.string :county_state, null: false
      t.string :city, null: false
      t.string :facility_name, null: false
      t.float :lat, null: false
      t.float :lon, null: false

      t.timestamps
    end
  end
end
